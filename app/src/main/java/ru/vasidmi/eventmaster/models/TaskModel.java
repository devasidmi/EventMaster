package ru.vasidmi.eventmaster.models;


import com.google.firebase.Timestamp;
import com.google.firebase.firestore.GeoPoint;

public class TaskModel {

    private String fullname;
    private String summary;
    private String comment;
    private Timestamp startTime;
    private Timestamp endTime;
    private GeoPoint startGeo;
    private GeoPoint endGeo;
    private String Id;

    public TaskModel() {
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public GeoPoint getStartGeo() {
        return startGeo;
    }

    public void setStartGeo(GeoPoint startGeo) {
        this.startGeo = startGeo;
    }

    public GeoPoint getEndGeo() {
        return endGeo;
    }

    public void setEndGeo(GeoPoint endGeo) {
        this.endGeo = endGeo;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}