package ru.vasidmi.eventmaster.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.firebase.firestore.GeoPoint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import ru.vasidmi.eventmaster.Database;
import ru.vasidmi.eventmaster.HeadActivity;
import ru.vasidmi.eventmaster.R;
import ru.vasidmi.eventmaster.models.TaskModel;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ViewHolder> {

    private Context context;
    private int tasksId;
    private List<TaskModel> taskModels;

    public TasksAdapter(Context context, int tasksId, List<TaskModel> taskModels) {
        this.context = context;
        this.tasksId = tasksId;
        this.taskModels = taskModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = (tasksId == 0 ? getActiveView(parent) : getDoneView(parent));
        return new ViewHolder(mView);
    }

    private View getActiveView(ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.task_item_active, parent, false);
    }

    private View getDoneView(ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.task_item_done, parent, false);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DateFormat mDateFormat = new SimpleDateFormat("dd.MM.yyyy H:mm:ss");
        Date startDate = null;
        switch (tasksId) {
            case 0:
                holder.fullname.setText(taskModels.get(position).getFullname());
                startDate = taskModels.get(position).getStartTime().toDate();
                holder.startTime.setText("Начало: " + mDateFormat.format(startDate));
                break;
            case 1:
                holder.fullname.setText(taskModels.get(position).getFullname());
                startDate = taskModels.get(position).getStartTime().toDate();
                Date endDate = taskModels.get(position).getEndTime().toDate();
                holder.startTime.setText("Начало: " + mDateFormat.format(startDate));
                holder.endTime.setText("Завершение: " + mDateFormat.format(endDate));
                break;
        }
    }

    public void refreshList(List<TaskModel> tasks) {
        taskModels = tasks;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return taskModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fullname)
        AppCompatTextView fullname;
        @Nullable
        @BindView(R.id.startTime)
        AppCompatTextView startTime;
        @Nullable
        @BindView(R.id.endTime)
        AppCompatTextView endTime;
        @BindView(R.id.task_item)
        public MaterialRippleLayout foreground;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Optional
        @OnClick(R.id.deleteBtn)
        void deleteTask() {
            final MaterialDialog mdb = new MaterialDialog.Builder(context)
                    .title("Удаление задания")
                    .content("Вы действительно хотите удалить задание?")
                    .positiveText("Да")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            new Database(context).deleteTask(taskModels.get(getAdapterPosition()).getId());
                        }
                    })
                    .negativeText("Отмена")
                    .cancelable(true)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .build();
            mdb.show();
        }

        @OnClick(R.id.task_item)
        void onTaskClicked() {
            Intent mIntent = new Intent();
            String summary = taskModels.get(getAdapterPosition()).getSummary();
            String comment = taskModels.get(getAdapterPosition()).getComment();
            mIntent.putExtra("comment", comment);
            mIntent.putExtra("summary", summary);
            switch (tasksId) {
                case 0:
                    GeoPoint geo = taskModels.get(getAdapterPosition()).getStartGeo();
                    mIntent.putExtra("lat", geo.getLatitude());
                    mIntent.putExtra("ltd", geo.getLongitude());
                    mIntent.setAction(HeadActivity.GEO_DIALOG);
                    context.sendBroadcast(mIntent);
                    break;
                case 1:
                    GeoPoint startGeo = taskModels.get(getAdapterPosition()).getStartGeo();
                    GeoPoint endGeo = taskModels.get(getAdapterPosition()).getEndGeo();
                    mIntent.putExtra("startLat", startGeo.getLatitude());
                    mIntent.putExtra("startLtd", startGeo.getLongitude());
                    mIntent.putExtra("endLat", endGeo.getLatitude());
                    mIntent.putExtra("endLtd", endGeo.getLongitude());
                    mIntent.setAction(HeadActivity.GEO_DIALOG);
                    context.sendBroadcast(mIntent);
                    break;
            }
        }
    }
}
