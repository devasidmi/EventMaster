package ru.vasidmi.eventmaster;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vasidmi.eventmaster.fragments.PerformerFragment;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private static final String PERF = "Исполнитель";
    private static final String ADMIN = "Руководитель";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupUI();
    }

    private void setupUI() {

        Intent mIntent = getIntent();
        if (mIntent != null) {
            if (mIntent.getExtras() != null) {
                String title = (String) mIntent.getExtras().get(LoginActivity.TITLE);
                if (title != null) {
                    mToolbar.setTitle(title);
                    openScreen(title);
                }
            }
        }
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void openScreen(String title) {
        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        switch (title) {
            case ADMIN:
                startActivity(new Intent(this, HeadActivity.class));
                break;
            case PERF:
                mFragmentTransaction.replace(R.id.fragment_container, new PerformerFragment(), PerformerFragment.TAG);
                break;
        }
        mFragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return false;
        }
    }
}
