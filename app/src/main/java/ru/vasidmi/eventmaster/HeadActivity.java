package ru.vasidmi.eventmaster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;
import ru.vasidmi.eventmaster.fragments.TasksListFragment;

public class HeadActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, MaterialTabListener, OnMapReadyCallback {

    public static final String GEO_DIALOG = "GEO_DIALOG";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tabs)
    MaterialTabHost tabs;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    private MaterialDialog geoDialog;
    private String[] titles = {"Активные", "Завершённые"};
    private BroadcastReceiver mBroadcastReceiver;
    private SupportMapFragment map;
    private double lat, ltd, startLat, startLtd, endLat, endLtd;
    private String summaryValue, commentValue;
    private AppCompatTextView summary, comment;
    private View geoVeiew;
    private int taskId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_head);
        ButterKnife.bind(this);
        setupListener();
        setupUI();
    }

    private void setupListener() {
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(GEO_DIALOG);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                summaryValue = intent.getStringExtra("summary");
                commentValue = intent.getStringExtra("comment");
                if (!intent.hasExtra("endLat")) {
                    lat = intent.getDoubleExtra("lat", -1.0);
                    ltd = intent.getDoubleExtra("ltd", -1.0);
                    showStartGeoDialog();
                } else {
                    startLat = intent.getDoubleExtra("startLat", -1.0);
                    startLtd = intent.getDoubleExtra("startLtd", -1.0);
                    endLat = intent.getDoubleExtra("endLat", -1.0);
                    endLtd = intent.getDoubleExtra("endLtd", -1.0);
                    taskId = 1;
                    showEndGeoDialog();
                }
            }
        };
        this.registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    private void showStartGeoDialog() {
        geoDialog = new MaterialDialog.Builder(this)
                .cancelable(true)
                .customView(geoVeiew, true)
                .positiveText("Закрыть")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        geoDialog.dismiss();
                    }
                }).dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        geoDialog = null;
                    }
                })
                .build();
        summary.setText("Описание: " + summaryValue);
        comment.setText("Комментарий: " + commentValue);
        geoDialog.show();
        map.getMapAsync(this);
    }

    private void showEndGeoDialog() {

        geoDialog = new MaterialDialog.Builder(this)
                .cancelable(true)
                .customView(geoVeiew, true)
                .positiveText("Закрыть")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        geoDialog.dismiss();
                    }
                }).dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        geoDialog = null;
                    }
                })
                .build();
        summary.setText("Описание: " + summaryValue);
        comment.setText("Комментарий: " + commentValue);
        geoDialog.show();
        map.getMapAsync(this);
    }

    private void setupUI() {
        mToolbar.setTitle(R.string.admin);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mViewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        mViewPager.addOnPageChangeListener(this);
        setupTabs();
        geoVeiew = LayoutInflater.from(this).inflate(R.layout.route_info, null, false);
        summary = geoVeiew.findViewById(R.id.summary);
        comment = geoVeiew.findViewById(R.id.comment);
        map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map2);
    }

    private void setupTabs() {
        if (mViewPager.getAdapter() != null) {
            for (int p = 0; p < mViewPager.getAdapter().getCount(); ++p) {
                tabs.addTab(tabs.newTab()
                        .setText(titles[p]).setTabListener(this));
            }
        }

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        tabs.setSelectedNavigationItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(MaterialTab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        GoogleMap mGoogleMap = googleMap;
        mGoogleMap.clear();
        if (taskId == 1) {
            LatLng startLatLng = new LatLng(startLat, startLtd);
            LatLng endLatLng = new LatLng(endLat, endLtd);
            mGoogleMap.addMarker(new MarkerOptions()
                    .position(startLatLng).title("Начало"));
            mGoogleMap.addMarker(new MarkerOptions()
                    .position(endLatLng).title("Завершено"));
            mGoogleMap.addPolyline(new PolylineOptions().add(startLatLng, endLatLng).width(5).color(Color.RED));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLatLng, 18));
        } else {
            LatLng latLng = new LatLng(lat, ltd);
            mGoogleMap.addMarker(new MarkerOptions()
                    .position(latLng).title("Начало"));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
        }
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TasksListFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return titles.length;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(this);
    }
}
