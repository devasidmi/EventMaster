package ru.vasidmi.eventmaster;

import android.content.Context;
import android.content.SharedPreferences;

public class Settings {

    private Context context;

    public Settings(Context context) {
        this.context = context;
    }

    public void saveActveTask(String fullname, String id, String startTime, String summary, String comment) {
        if (!isActiveTask()) {
            SharedPreferences mSharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
            SharedPreferences.Editor mEditor = mSharedPreferences.edit();
            mEditor.putString("fullname", fullname);
            mEditor.putString("id", id);
            mEditor.putString("startTime", startTime);
            mEditor.putString("summary", summary);
            mEditor.putString("comment", comment);
            mEditor.apply();
        }
    }

    public boolean isActiveTask() {
        SharedPreferences mSharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        return mSharedPreferences.contains("id");
    }

    public void clearActiveTask() {
        if (isActiveTask()) {
            SharedPreferences mSharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
            SharedPreferences.Editor mEditor = mSharedPreferences.edit();
            mEditor.remove("fullname");
            mEditor.remove("id");
            mEditor.remove("startTime");
            mEditor.remove("summary");
            mEditor.remove("comment");
            mEditor.apply();
        }
    }

    public String getActiveTaskId() {
        String id = null;
        if (isActiveTask()) {
            SharedPreferences mSharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
            id = mSharedPreferences.getString("id", null);
        }
        return id;
    }

    public String getActiveTaskUser() {
        String fullname = null;
        if (isActiveTask()) {
            SharedPreferences mSharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
            fullname = mSharedPreferences.getString("fullname", null);
        }
        return fullname;
    }

    public String getStartTime() {
        String startTime = null;
        if (isActiveTask()) {
            SharedPreferences mSharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
            startTime = mSharedPreferences.getString("startTime", null);
        }
        return startTime;
    }

    public String getActiveTaskSummary() {
        String summary = null;
        if (isActiveTask()) {
            SharedPreferences mSharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
            summary = mSharedPreferences.getString("summary", null);
        }
        return summary;
    }

    public String getActiveTaskComment() {
        String comment = null;
        if (isActiveTask()) {
            SharedPreferences mSharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
            comment = mSharedPreferences.getString("comment", null);
        }
        return comment;
    }
}
