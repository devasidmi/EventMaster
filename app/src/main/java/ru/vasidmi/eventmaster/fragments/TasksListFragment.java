package ru.vasidmi.eventmaster.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vasidmi.eventmaster.Database;
import ru.vasidmi.eventmaster.R;
import ru.vasidmi.eventmaster.adapters.TasksAdapter;
import ru.vasidmi.eventmaster.models.TaskModel;

public class TasksListFragment extends Fragment {

    @BindView(R.id.tasks_list)
    RecyclerView mTasksList;

    private View mView;
    private TasksAdapter mTasksAdapter;
    private int tasksId;
    private List<TaskModel> taskModels;
    private FirebaseFirestore store;
    private Database db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.tasks_list, container, false);
        db = new Database(getContext());
        store = db.getDb();
        ButterKnife.bind(this, mView);
        if (getArguments() != null) {
            tasksId = getArguments().getInt("taskId");
        }
        switch (tasksId) {
            case 0:
                initActiveTasksListener();
                break;
            case 1:
                initDoneTasksListener();
                break;
        }
        return mView;
    }


    private void initActiveTasksListener() {
        store.collection("active").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                if (e == null) {
                    List<TaskModel> tasks = queryDocumentSnapshots.toObjects(TaskModel.class);
                    List<DocumentSnapshot> docs = queryDocumentSnapshots.getDocuments();
                    for (int i = 0; i < docs.size(); ++i) {
                        tasks.get(i).setId(docs.get(i).getId());
                    }
                    setupUI(tasks);
                } else {
                    Log.e("YAY!", "error on initTasksListener");
                }

            }
        });
    }

    private void initDoneTasksListener() {
        store.collection("done").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                if (e == null) {
                    List<TaskModel> tasks = queryDocumentSnapshots.toObjects(TaskModel.class);
                    List<DocumentSnapshot> docs = queryDocumentSnapshots.getDocuments();
                    for (int i = 0; i < docs.size(); ++i) {
                        tasks.get(i).setId(docs.get(i).getId());
                    }
                    setupUI(tasks);
                } else {
                    Log.e("YAY!", "error on initTasksListener");
                }

            }
        });
    }


    private void setupUI(List<TaskModel> tasks) {
        if (mTasksAdapter == null) {
            mTasksAdapter = new TasksAdapter(getContext(), tasksId, tasks);
            mTasksList.setLayoutManager(new LinearLayoutManager(getContext()));
            mTasksList.setAdapter(mTasksAdapter);
        } else {
            mTasksAdapter.refreshList(tasks);
        }
    }

    public static TasksListFragment newInstance(int taskId) {

        Bundle args = new Bundle();
        args.putInt("taskId", taskId);

        TasksListFragment fragment = new TasksListFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
