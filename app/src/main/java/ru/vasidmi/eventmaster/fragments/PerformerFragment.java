package ru.vasidmi.eventmaster.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.arasthel.asyncjob.AsyncJob;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.GeoPoint;
import com.joestelmach.natty.Parser;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;
import ru.vasidmi.eventmaster.Database;
import ru.vasidmi.eventmaster.R;
import ru.vasidmi.eventmaster.Settings;


public class PerformerFragment extends Fragment implements OnMapReadyCallback {

    public static final String TAG = "PerformerFragment";
    public static final String START_ENABLE_DISABLE = "startEnable";
    public static final String STOP_ENABLE_DISABLE = "stopEnable";
    public static final String CLEAR_ACTIVE_TASK = "clearActiveTask";
    public static final String SHOW_DISMISS_DIALOG = "showDismissDialog";
    //    public static final String SHOW_DISMISS_START_TIME = "showDismissStartTime";
    private static final String LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String LOCATION_PERMISSION2 = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String SAVE_DATA = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    SupportMapFragment mapFragment;
    private Context mContext;
    private GoogleMap mGoogleMap;
    private LocationGooglePlayServicesProvider provider;
    private Database mDatabase;
    private Settings mSettings;
    private SmartLocation smartLocation;
    private BroadcastReceiver mBroadcastReceiver;
    private MaterialDialog mDialog;

    @BindView(R.id.fullname)
    MaterialEditText fullname;
    @BindView(R.id.summary)
    MaterialEditText summary;
    @BindView(R.id.comment)
    MaterialEditText comment;
    @BindView(R.id.startEventBtn)
    AppCompatButton startBtn;
    @BindView(R.id.stopEventBtn)
    AppCompatButton stopBtn;
    @BindView(R.id.startTime)
    AppCompatTextView startTime;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.performer_fragment, container, false);
        mContext = getContext();
        mDatabase = new Database(mContext);
        mSettings = new Settings(mContext);
        ButterKnife.bind(this, mView);
        checkActiveTask();
        setupListener();
        smartLocation = new SmartLocation.Builder(mContext).build();
        provider = new LocationGooglePlayServicesProvider();
        provider.setCheckLocationSettings(true);
        AsyncJob.doInBackground(new AsyncJob.OnBackgroundJob() {
            @Override
            public void doOnBackground() {
                setupUI();
            }
        });
        ButterKnife.bind(this, mView);
        return mView;
    }

    private void checkActiveTask() {
        if (mSettings.isActiveTask()) {
            fullname.setText(mSettings.getActiveTaskUser());
            summary.setText(mSettings.getActiveTaskSummary());
            comment.setText(mSettings.getActiveTaskComment());
            showHideStartTime();
            setEnableStartBtn();
            setEnableStopBtn();
            setEnableFullnameField();
            setEnableSummaryField();
            setEnableCommentField();
        }
    }

    private void setupListener() {
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(START_ENABLE_DISABLE);
        mIntentFilter.addAction(STOP_ENABLE_DISABLE);
        mIntentFilter.addAction(CLEAR_ACTIVE_TASK);
        mIntentFilter.addAction(SHOW_DISMISS_DIALOG);
//        mIntentFilter.addAction(SHOW_DISMISS_START_TIME);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction() != null) {
                    switch (intent.getAction()) {
                        case START_ENABLE_DISABLE:
                            setEnableStartBtn();
                            setEnableFullnameField();
                            setEnableSummaryField();
                            setEnableCommentField();
                            showHideStartTime();
                            break;
                        case STOP_ENABLE_DISABLE:
                            setEnableStopBtn();
                            break;
                        case CLEAR_ACTIVE_TASK:
                            setEnableStartBtn();
                            setEnableStopBtn();
                            setEnableFullnameField();
                            setEnableSummaryField();
                            setEnableCommentField();
                            clearFullnameField();
                            clearSummaryField();
                            clearCommentField();
                            startTime.setText("");
                            startTime.setVisibility(View.GONE);
                            mSettings.clearActiveTask();
                            break;
                        case SHOW_DISMISS_DIALOG:
                            int content = intent.getIntExtra("content", -1);
                            showDismissDialog(content);
                            break;
                    }
                }
            }
        };
        mContext.registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    private void destroyListener() {
        mContext.unregisterReceiver(mBroadcastReceiver);
    }

    private void setupUI() {
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        if (!checkPermissions()) {
            askPermission();
        } else {
            if (mapFragment != null) {
                AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                    @Override
                    public void doInUIThread() {
                        setupMap();
                    }
                });
            }
        }
    }

    private boolean checkPermissions() {
        return ContextCompat.checkSelfPermission(mContext, LOCATION_PERMISSION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mContext, LOCATION_PERMISSION2) == PackageManager.PERMISSION_GRANTED;
    }

    private void askPermission() {
        String[] permissions = {LOCATION_PERMISSION, LOCATION_PERMISSION2, SAVE_DATA};
        this.requestPermissions(permissions, 1);
    }

    private void getLocation() {
        final MaterialDialog mdb = new MaterialDialog.Builder(getContext())
                .content("Получаем местоположение...")
                .cancelable(false)
                .progress(true, 0)
                .build();
        mdb.show();
        smartLocation.location(provider).config(LocationParams.NAVIGATION).oneFix().start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                if (mGoogleMap != null) {
                    mdb.dismiss();
                    double lat, lng;
                    lat = location.getLatitude();
                    lng = location.getLongitude();
                    LatLng pos = new LatLng(lat, lng);
                    mGoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lng)));
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 18)); //18 best
                }
            }
        });
    }

    private void showDismissDialog(int content) {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
            return;
        }
        mDialog = initDialog(content);
        mDialog.show();
    }

    private void setupMap() {
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        getLocation();
    }

    private void setEnableStartBtn() {
        startBtn.setEnabled(!startBtn.isEnabled());
    }

    private void setEnableStopBtn() {
        stopBtn.setEnabled(!stopBtn.isEnabled());
    }

    private void setEnableFullnameField() {
        fullname.setEnabled(!fullname.isEnabled());
        if (!fullname.isEnabled()) {
            fullname.setShowClearButton(false);
        } else {
            fullname.setShowClearButton(true);
        }

    }

    private void setEnableSummaryField() {
        summary.setEnabled(!summary.isEnabled());
        if (!summary.isEnabled()) {
            summary.setShowClearButton(false);
        } else {
            summary.setShowClearButton(true);
        }

    }

    private void setEnableCommentField() {
        comment.setEnabled(!comment.isEnabled());
        if (!comment.isEnabled()) {
            comment.setShowClearButton(false);
        } else {
            comment.setShowClearButton(true);
        }

    }

    private void clearFullnameField() {
        fullname.setText("");
    }

    private void clearSummaryField() {
        summary.setText("");
    }

    private void clearCommentField() {
        comment.setText("");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == 0 && grantResults[1] == 0) {
            if (mapFragment != null)
                mapFragment.getMapAsync(this);
        } else {
            if (getActivity() != null)
                getActivity().finish();
        }
    }

    private boolean checkInput() {
        boolean isValid = false;
        if (fullname.getText().toString().trim().equals("") || fullname.getText().toString().isEmpty()) {
            fullname.setError("Необходимо заполнить это поле");
        } else if (summary.getText().toString().trim().equals("") || summary.getText().toString().isEmpty()) {
            summary.setError("Необходимо заполнить это поле");
        } else if (comment.getText().toString().trim().equals("") || comment.getText().toString().isEmpty()) {
            comment.setError("Необходимо заполнить это поле");
        } else {
            isValid = true;
        }
        return isValid;
    }

    private Timestamp getDate() {
        Date mDate = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow")).getTime();
        return new Timestamp(mDate);
    }

    private MaterialDialog initDialog(int content) {
        MaterialDialog mdb = new MaterialDialog.Builder(mContext)
                .cancelable(false)
                .content(content)
                .progress(true, 0).build();
        return mdb;
    }

    private void showHideStartTime() {
        if (startTime.getVisibility() == View.VISIBLE) {
            startTime.setVisibility(View.GONE);
        } else {
            startTime.setVisibility(View.VISIBLE);
            String startDate = null;
            Parser parser = new Parser();
            String time = mSettings.getStartTime();
            final DateFormat mDateFormat = new SimpleDateFormat("dd.MM.yyyy H:mm:ss");
            startDate = mDateFormat.format(parser.parse(time).get(0).getDates().get(0));
            startTime.setText("Задание началось: " + startDate);
        }
    }

    @OnClick(R.id.startEventBtn)
    void startEvent() {
        if (checkInput()) {
            showDismissDialog(R.string.add_active_task);
            smartLocation.location(provider).config(LocationParams.NAVIGATION).oneFix().start(new OnLocationUpdatedListener() {
                @Override
                public void onLocationUpdated(Location location) {
                    double lat, ltd;
                    lat = location.getLatitude();
                    ltd = location.getLongitude();
                    String fullnameValue = fullname.getText().toString().trim();
                    String summaryValue = summary.getText().toString().trim();
                    String commentValue = comment.getText().toString().trim();
                    fullname.setText(fullnameValue);
                    mDatabase.addActiveTask(getDate(), fullnameValue, summaryValue, commentValue, new GeoPoint(lat, ltd));
                }
            });
        }
    }

    @OnClick(R.id.stopEventBtn)
    void stopEvent() {
        showDismissDialog(R.string.add_done_task);
        smartLocation.location(provider).config(LocationParams.NAVIGATION).oneFix().start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                double lat, ltd;
                String id;
                lat = location.getLatitude();
                ltd = location.getLongitude();
                id = mSettings.getActiveTaskId();
                mDatabase.addDoneTask(id, getDate(), new GeoPoint(lat, ltd));
            }
        });
    }

    private void dismissKeyBoard() {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(fullname.getWindowToken(), 0);
    }

    @OnClick(R.id.map)
    void onMapClick() {
        dismissKeyBoard();
    }

    @OnClick(R.id.performer_fragment_content)
    void onContentClick() {
        dismissKeyBoard();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("YAY!", "onDestroy");
        if (mBroadcastReceiver != null) {
            destroyListener();
            mBroadcastReceiver = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("YAY!", "onPause");
        destroyListener();
        mBroadcastReceiver = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("YAY!", "onResume");
        if (mBroadcastReceiver == null)
            setupListener();
    }
}
