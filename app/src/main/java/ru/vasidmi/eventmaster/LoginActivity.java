package ru.vasidmi.eventmaster;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public static final String TITLE = "title";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setupUI();
    }

    private void setupUI() {
        mToolbar.setTitle(R.string.login);
        setSupportActionBar(mToolbar);
    }

    @OnClick(R.id.performerBtn)
    void onPerformerBtnClicked() {
        startActivity(new Intent(this, MainActivity.class).putExtra(TITLE, this.getResources().getString(R.string.performer)));
    }

    @OnClick(R.id.adminBtn)
    void onAdminBtnClicked() {
        startActivity(new Intent(this, MainActivity.class).putExtra(TITLE, this.getResources().getString(R.string.admin)));
    }

}
