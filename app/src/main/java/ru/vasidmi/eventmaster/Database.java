package ru.vasidmi.eventmaster;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

import java.util.HashMap;
import java.util.Map;

import ru.vasidmi.eventmaster.fragments.PerformerFragment;

public class Database {

    private FirebaseFirestore db;
    private Context context;

    public FirebaseFirestore getDb() {
        return db;
    }

    public Database(Context context) {
        db = FirebaseFirestore.getInstance();
        this.context = context;
    }

    public void addActiveTask(final Timestamp datetime, final String fullname, final String summary, final String comment, GeoPoint geo) {

        Map<String, Object> mActiveTask = new HashMap<>();
        mActiveTask.put("startTime", datetime);
        mActiveTask.put("fullname", fullname);
        mActiveTask.put("summary", summary);
        mActiveTask.put("comment", comment);
        mActiveTask.put("startGeo", geo);

        String collection_active = this.context.getResources().getString(R.string.collection_active);
        final Intent showDismissDialogIntent = new Intent();
        showDismissDialogIntent.setAction(PerformerFragment.SHOW_DISMISS_DIALOG);
        showDismissDialogIntent.putExtra("content", R.string.add_active_task);
        db.collection(collection_active).add(mActiveTask).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                String id = task.getResult().getId();
                new Settings(context).saveActveTask(fullname, id, datetime.toDate().toString(), summary, comment);
                Intent startIntent = new Intent();
                Intent stopIntent = new Intent();
                startIntent.setAction(PerformerFragment.START_ENABLE_DISABLE);
                stopIntent.setAction(PerformerFragment.STOP_ENABLE_DISABLE);
                context.sendBroadcast(startIntent);
                context.sendBroadcast(stopIntent);
                context.sendBroadcast(showDismissDialogIntent);
                Toast.makeText(context, R.string.start_task, Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("YAY!", "error on addActiveTask");
                context.sendBroadcast(showDismissDialogIntent);
                Toast.makeText(context, "Ошибка", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void addDoneTask(final String id, final Timestamp datetime, final GeoPoint geo) {
        final String collection_active = context.getResources().getString(R.string.collection_active);
        final String collection_done = context.getResources().getString(R.string.collection_done);
        final Intent showDismissDialog = new Intent();
        showDismissDialog.setAction(PerformerFragment.SHOW_DISMISS_DIALOG);
        showDismissDialog.putExtra("content", R.string.add_done_task);
        db.collection(collection_active).document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot activeTask = task.getResult();
                String fullname = activeTask.getString("fullname");
                String summary = activeTask.getString("summary");
                String comment = activeTask.getString("comment");
                GeoPoint startGeo = activeTask.getGeoPoint("startGeo");
                Timestamp startTime = activeTask.getTimestamp("startTime");
                final Map<String, Object> mDoneTask = new HashMap<>();
                mDoneTask.put("startTime", startTime);
                mDoneTask.put("fullname", fullname);
                mDoneTask.put("summary", summary);
                mDoneTask.put("comment", comment);
                mDoneTask.put("endTime", datetime);
                mDoneTask.put("startGeo", startGeo);
                mDoneTask.put("endGeo", geo);
                db.collection(collection_active).document(id).delete().addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("YAY!", "error on delete active task");
                        Toast.makeText(context, "Ошибка", Toast.LENGTH_SHORT).show();
                    }
                });
                db.collection(collection_done).add(mDoneTask).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        Intent сlearTaskIntent = new Intent();
                        сlearTaskIntent.setAction(PerformerFragment.CLEAR_ACTIVE_TASK);
                        context.sendBroadcast(сlearTaskIntent);
                        context.sendBroadcast(showDismissDialog);
                        Toast.makeText(context, R.string.stop_task, Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("YAY!", "error on addDoneTask on adding new data");
                        context.sendBroadcast(showDismissDialog);
                        Toast.makeText(context, "Ошибка", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("YAY!", "error on addDoneTask get old task");
                context.sendBroadcast(showDismissDialog);
                Toast.makeText(context, "Ошибка", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void deleteTask(String id) {
        db.collection("done").document(id).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.e("YAY!", "deleted");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("YAY!", "error on deleteTask");
            }
        });
    }

}
